# SamplePodCreation

[![CI Status](https://img.shields.io/travis/kirankrify/SamplePodCreation.svg?style=flat)](https://travis-ci.org/kirankrify/SamplePodCreation)
[![Version](https://img.shields.io/cocoapods/v/SamplePodCreation.svg?style=flat)](https://cocoapods.org/pods/SamplePodCreation)
[![License](https://img.shields.io/cocoapods/l/SamplePodCreation.svg?style=flat)](https://cocoapods.org/pods/SamplePodCreation)
[![Platform](https://img.shields.io/cocoapods/p/SamplePodCreation.svg?style=flat)](https://cocoapods.org/pods/SamplePodCreation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SamplePodCreation is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SamplePodCreation'
```

## Author

kirankrify, alekhya@krify.com

## License

SamplePodCreation is available under the MIT license. See the LICENSE file for more info.
